# Mastodon kustomize deploy

## Deploy

    kustomize build overlays/your-env | kubectl -n mastodon apply -f -

## Run tootctl

    kubectl exec -n mastodon -it deployment/web -- tootctl

## Upgrade example

Remember to backup the DB before upgrading.

### Postgres-operator trigger backup

With PostgresCluster.spec.backups.pgbackrest.manual configured, all you should have to do to trigger a full backup is run this.

    kubectl annotate -n postgres-operator postgrescluster <cluster name> \
        --overwrite postgres-operator.crunchydata.com/pgbackrest-backup="$(date -Ins)"

### Follow release Upgrade notes

This is an example of the instructions for the [v3.4.0 release](https://github.com/mastodon/mastodon/releases/tag/v3.4.0).

    kubectl exec -n mastodon -it deployment/web -- env SKIP_POST_DEPLOYMENT_MIGRATIONS=true \
        bundle exec rails db:migrate

>Pre-upgrade step like this is not necessary for every upgrade.

### Re-deploy with new image tag for your new release

Edit ``base/setup/kustomization.yaml`` to bump the version of Mastodon, and push to git to trigger deploy. When finished, run this.

    kubectl exec -n mastodon -it deployment/web -- tootctl cache clear
    kubectl exec -n mastodon -it deployment/web -- rails db:migrate

# SOPs

## Trigger CronJob manually

When you already have a CronJob defined you can trigger it by creating a Job from it.

    kubectl -n mastodon create job --from=cronjob/rebuild-feeds \
        "rebuild-feeds-manual-$(date +'%Y%m%d.%H%M%S')"

## Rebuild search index

If ES storage is persistent this should not be a problem but here is how to do it.

    kubectl exec -n mastodon -it deployment/web -- tootctl search deploy

# See also

* [tootctl docs](https://docs.joinmastodon.org/admin/tootctl/)
